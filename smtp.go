package sdk

import (
	"errors"
	"net/smtp"
)

type SMTP struct {
	TLS      bool
	Author   []string
	Sender   SenderInfo
	Receiver ReceiverInfo
	Host     *url
}

var MailServer = &MServer{
	GMail: url{"smtp.gmail.com", "587"},
	Yahoo: url{"smtp.mail.yahoo.com","587"},
}

type MServer struct {
	GMail url
	Yahoo url
}

type url struct {
	Domain string
	Port   string
}

type SenderInfo struct {
	Email, Content, Title, Username, Password string
}

type ReceiverInfo struct {
	Email, Content, Title string
}

func (m *SMTP) SendMail(content string) error {
	if m.Host == nil {
		return errors.New("Host is invalid")
	}

	host := m.Host.Domain + ":" + m.Host.Port
	emailAuth := smtp.PlainAuth("", m.Sender.Username, m.Sender.Password, m.Host.Domain)
	if emailAuth == nil {
		return errors.New("Authorize failed")
	}
	err := smtp.SendMail(host, emailAuth, m.Sender.Email, []string{m.Receiver.Email}, []byte(content))
	return err
}
