package sdk

type Server struct {
	Addr             string
	Port             int
	Protocol         string
	ID               int
	preRequestHandle Handler
	beforeResponse   interface{}
	options			[]interface{}
	ServerName		string
}

type System struct {
	listServer []*Server
	listDB     []*DBModel
	AppName		string
}