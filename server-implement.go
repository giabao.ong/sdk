package sdk

import (
	"errors"
	"fmt"
	"net/http"
	"strconv"
	"sync"
	"time"
)

func (s *System) CreateServerWithHttp(serve *Server) error {
	if serve == nil {
		panic("Server nil")
	}

	serve.ID = len(s.listServer) + 1
	s.listServer = append(s.listServer, serve)

	return nil
}


func (s *System) DBInit(conf *ConfigDB, cb CallBack) error {
	if conf == nil {
		panic("Config DB nill")
	}
	if conf.DBName == "" {
		panic(errors.New("Invalid config DB"))
	}
	if conf.Port <= 0 {
		conf.Port = 27017
	}

	a := ""
	if conf.Password != "" || conf.User != "" {
		a = ":@"
	}

	url := fmt.Sprintf("mongodb://%v%v%v%v:%v/%v?readPreference=primary&ssl=false",
		conf.User, conf.Password, a, conf.Addr, conf.Port, conf.DBName)

	db := &DBModel{
		DBName:   conf.DBName,
		url:      url,
		callback: cb,
	}
	s.listDB = append(s.listDB, db)
	return nil
}

func (ser *Server) start(wg *sync.WaitGroup) error {
	if ser.Addr == "" {
		ser.Addr = ":"
	}

	if ser.Port < 0 {
		panic("Port < 0")
	}

	addr := ser.Addr + strconv.Itoa(ser.Port)
	s := &http.Server{
		Addr:         addr,
		ReadTimeout:  10 * time.Second,
		WriteTimeout: 10 * time.Second,
	}
	err := s.ListenAndServe()
	if err != nil {
		wg.Done()
		panic(err)
	}
	return err
}

func (s *System) Launch() {
	println("+ Launching...")
	group := sync.WaitGroup{}
	for i, _ :=range s.listDB {
		println(fmt.Sprintf("+ Connecting DB: %v", s.listDB[i].DBName))
		err := s.listDB[i].connect()
		if err != nil {
			println(err.Error())
		}
	}
	group.Add(1)
	for i, _ := range s.listServer {
		name := s.listServer[i].ServerName
		if s.listServer[i].ServerName == "" {
			name = strconv.Itoa(s.listServer[i].ID)
		}
		println(fmt.Sprintf("=====> Server %v starting at: %v ", name, s.listServer[i].Port))
		go s.listServer[i].start(&group)
	}
	group.Wait()
	return
}