package sdk

import (
	"net/http"
)

type apiModel struct {
	w           http.ResponseWriter
	req         *http.Request
}

type apiStatus struct {
	Ok           string
	NotFound     string
	Unauthorized string
	Invalid      string
	Error        string
}

var APIStatus = &apiStatus{
	Ok:           "OK",
	NotFound:     "NOT_FOUND",
	Unauthorized: "UNAUTHORIZED",
	Invalid:      "INVALID",
	Error:        "ERROR",
}

type Response struct {
	Status    string            `json:"status"`
	Data      interface{}       `json:"data"`
	Message   string            `json:"message"`
	ErrorCode string            `json:"errorCode"`
	Header    map[string]string `json:"-"`
	Total	int					`json:"total"`
}

type APIResponse interface {
	Resp(*Response) error
}

type APIRequest interface {
	GetBody() string
	ParseBodyTo(interface{}) error
	GetParam(string) string
	GetUserAgent() string
	GetHeader(string) string
	GetHeaders() map[string]string
}

type Handler = func(req APIRequest, res APIResponse) error
type ProcessFunc = func(options...interface{})