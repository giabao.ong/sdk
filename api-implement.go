package sdk

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"path/filepath"
	"strings"
)

func (s *Server)  Delete (url string, handler Handler) {
	http.HandleFunc(url, func(writer http.ResponseWriter, request *http.Request) {
		s.middle(writer,request)
		if request.Method != http.MethodDelete {
			r := Response{
				Status: "NOT_ALLOWED",
				Data:   nil,
			}
			writer.WriteHeader(405)
			json.NewEncoder(writer).Encode(r)
			return
		}
		var req APIRequest
		req = &apiModel{
			w:   writer,
			req: request,
		}
		var res APIResponse
		res = &apiModel{
			w:   writer,
			req: request,
		}
		handler(req, res)
	})
}

func (s *Server)  Options (url string, handler Handler) {
	http.HandleFunc(url, func(writer http.ResponseWriter, request *http.Request) {
		s.middle(writer,request)
		if request.Method != http.MethodOptions {
			r := Response{
				Status: "NOT_ALLOWED",
				Data:   nil,
			}
			writer.WriteHeader(405)
			json.NewEncoder(writer).Encode(r)
			return
		}
		var req APIRequest
		req = &apiModel{
			w:   writer,
			req: request,
		}
		var res APIResponse
		res = &apiModel{
			w:   writer,
			req: request,
		}
		handler(req, res)
	})
}

func (s *Server)  Put (url string, handler Handler) {
	http.HandleFunc(url, func(writer http.ResponseWriter, request *http.Request) {
		s.middle(writer,request)
		if request.Method != http.MethodPut {
			r := Response{
				Status: "NOT_ALLOWED",
				Data:   nil,
			}
			writer.WriteHeader(405)
			json.NewEncoder(writer).Encode(r)
			return
		}
		var req APIRequest
		req = &apiModel{
			w:   writer,
			req: request,
		}
		var res APIResponse
		res = &apiModel{
			w:   writer,
			req: request,
		}
		handler(req, res)
	})
}


func (s *Server)  Post (url string, handler Handler) {
	http.HandleFunc(url, func(writer http.ResponseWriter, request *http.Request) {
		s.middle(writer,request)
		if request.Method != http.MethodPost {
			r := Response{
				Status: "NOT_ALLOWED",
				Data:   nil,
			}
			writer.WriteHeader(405)
			json.NewEncoder(writer).Encode(r)
			return
		}
		var req APIRequest
		req = &apiModel{
			w:   writer,
			req: request,
		}
		var res APIResponse
		res = &apiModel{
			w:   writer,
			req: request,
		}
		handler(req, res)
	})
}

func (s *Server) Get(url string, handler Handler) {
	http.HandleFunc(url, func(writer http.ResponseWriter, request *http.Request) {
		s.middle(writer,request)
		if request.Method != http.MethodGet {
			r := Response{
				Status: "NOT_ALLOWED",
				Data:   nil,
			}
			writer.WriteHeader(405)
			json.NewEncoder(writer).Encode(r)
			return
		}
		var req APIRequest
		api := &apiModel{
			w:   writer,
			req: request,
		}
		req = api
		var res APIResponse
		res = api
		handler(req, res)
	})
}

func (s *Server) PublicDir(url string, path string) {
	if path == "" {
		path = url
		if !strings.HasSuffix(".", path){
			path = "."+url
		}
	}
	http.HandleFunc(url,func(w http.ResponseWriter, r *http.Request) {
		if file := strings.TrimPrefix(r.URL.Path, url); len(file) < len(r.URL.Path) {

			target := path+file
			if !strings.HasSuffix(path, "/"){
				target = path+"/"+file
			}
			fmt.Println(target)
			ext := filepath.Ext(target)
			if ext == ".conf" || ext == ".go" || ext == ".ini" || ext == ".env" {
				r := &Response{
					Status: APIStatus.Unauthorized,
					Message: "Cannot access this file",
				}
				json.NewEncoder(w).Encode(r)
				return
			}
			bytes, err := ioutil.ReadFile(target)
			//ext
			if err != nil {
				r := &Response{
					Status: APIStatus.Error,
					Message: err.Error(),
					ErrorCode: "ERROR_READ_FILE",
				}
				json.NewEncoder(w).Encode(r)
				return
			}
			w.Write(bytes)
		} else {
			r := &Response{
				Status: APIStatus.NotFound,
				Message: "Not found",
				ErrorCode: "ERROR_READ_FILE",
			}
			json.NewEncoder(w).Encode(r)
			return
		}
	})}

func (a *apiModel) GetBody() string {
	bytes, err := ioutil.ReadAll(a.req.Body)
	if err != nil {
		return ""
	}
	defer a.req.Body.Close()
	return string(bytes)
}

func (a *apiModel) ParseBodyTo(i interface{}) error {
	bytes, err := ioutil.ReadAll(a.req.Body)
	if err != nil {
		return err
	}

	return json.Unmarshal(bytes, i)
}

func (a *apiModel) GetParam(q string) string {
	query := a.req.URL.Query()[q]
	return query[0]
}

func (a *apiModel) GetUserAgent() string {
	return a.req.UserAgent()
}

func (a *apiModel) GetHeader(key string) string {
	return a.req.Header.Get("key")
}

func (a *apiModel) GetHeaders() map[string]string {
	h := map[string]string{}
	rHeaders := a.req.Header
	for k := range rHeaders {
		h[k] = rHeaders.Get(k)
	}
	return h
}

func (a *apiModel) Resp(response *Response) error {
	if response.Header != nil {
		for i, v := range response.Header {
			a.w.Header().Add(i, v)
		}
	}

	a.w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	a.w.Header().Set("Access-Control-Allow-Headers",
		"Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
	a.w.Header().Set("Access-Control-Allow-Origin", "*")

	switch response.Status {
	case APIStatus.Ok:
		a.w.WriteHeader(200)
	case APIStatus.Error:
		a.w.WriteHeader(500)
	case APIStatus.Unauthorized:
		a.w.WriteHeader(401)
	case APIStatus.Invalid:
		a.w.WriteHeader(400)
	case APIStatus.NotFound:
		a.w.WriteHeader(404)
	default:
		a.w.WriteHeader(200)
	}
	return json.NewEncoder(a.w).Encode(response)
}

func (s *Server) PreRequest(handler Handler) error {
	if handler == nil {
		return errors.New("Handler is null")
	}

	s.preRequestHandle =handler
	return nil
}

func (s *Server) middle(w http.ResponseWriter, request *http.Request) {
	if s.preRequestHandle == nil {
		return
	}
	var req APIRequest
	req = &apiModel{
		w:   w,
		req: request,
	}
	var res APIResponse
	res = &apiModel{
		w:   w,
		req: request,
	}

	err := s.preRequestHandle(req, res)
	if err != nil {
		r := &Response{
			Status:    APIStatus.Error,
			Message:   err.Error(),
			ErrorCode: "MIDDLE_ERROR",
		}

		json.NewEncoder(w).Encode(r)
		return
	}
}