package sdk

import (
	"crypto/hmac"
	"crypto/sha256"
	"crypto/sha512"
	"encoding/base64"
	"encoding/json"
	"errors"
	"hash"
	"strings"
	"time"
)

var TypeAlg = &alg{
	HS256: &HashFunc{"HS256", sha256.New},
	HS384: &HashFunc{"HS384", sha512.New384},
}

type alg struct {
	HS256		*HashFunc
	HS384		*HashFunc
}

type HashFunc struct {
	Name	string
	Func	func() hash.Hash
}

type checkTime struct {
	Exp	int64
}

type JWT struct {
	Hash	*HashFunc
	header headerJWT
	Exp		*time.Duration
	Key		string
}

type headerJWT struct {
	Alg  string `json:"alg" bson:"-"`
	Type string `json:"typ" bson:"-"`
}

func (j *JWT) JWTEncoder(body map[string]interface{}) (string,error) {
	if len(j.Key) < 5 {
		return "",errors.New("Key is short, low security")
	}
	if j.Hash == nil {
		return "",errors.New("Header need define hash func")
	}

	if j.Hash.Func == nil {
		return "",errors.New("Header need define hash func")
	}

	header := j.header
	header.Type = "JWT"
	header.Alg = j.Hash.Name
	bytes, err := json.Marshal(header)
	if err != nil {
		return "", err
	}
	head := base64.URLEncoding.WithPadding(-1).EncodeToString(bytes)

	if j.Exp == nil {
		exp := time.Duration(60 *time.Minute)
		j.Exp = &exp
	}
	body["exp"] = time.Now().Add(*j.Exp).Unix()
	bytes, err = json.Marshal(body)
	if err != nil {
		return "", err
	}
	pay := base64.URLEncoding.WithPadding(-1).EncodeToString(bytes)

	temp := head+"."+pay

	sign := j.generateSign(temp)
	return temp+"."+sign, nil
}

func (j *JWT) ValidateToken(token string) error {
	if token == "" {
		return errors.New("Token is null")
	}
	if j.Hash == nil {
		return errors.New("Header need define hash func")
	}
	if j.Hash.Func == nil {
		return errors.New("Header need define hash func")
	}

	tk := strings.Split(token,".")
	if len(tk) < 0 {
		return errors.New("Token is invalid")
	}

	com := tk[0]+"."+tk[1]
	if 	sign := j.generateSign(com); sign != tk[2] {
		return errors.New("Invaild signature")
	}

	bytes,err := base64.URLEncoding.WithPadding(-1).DecodeString(tk[1])
	if err != nil {
		return err
	}

	body := checkTime{}
	err = json.Unmarshal(bytes, &body)
	if err != nil {
		return err
	}
	if body.Exp == 0{
		return errors.New("Token is expired")
	} else {
		t := time.Unix(body.Exp,0)
		if t.Before(time.Now()){
			return errors.New("Token is expired")
		}
	}

	return nil
}

func (j *JWT) generateSign(data string) string {
	sign := hmac.New(j.Hash.Func,[]byte(j.Key))
	sign.Write([]byte(data))
	return base64.URLEncoding.WithPadding(-1).EncodeToString(sign.Sum(nil))
}

func (j *JWT) ParseData(token string, result interface{}) error {
	if token == "" {
		return errors.New("Token is null")
	}

	tk := strings.Split(token,".")
	if len(tk) != 3 {
		return errors.New("Token is invalid")
	}
	bytes,err := base64.URLEncoding.WithPadding(-1).DecodeString(tk[1])
	if err != nil {
		return err
	}

	return json.Unmarshal(bytes, result)
}

//Refresh token by updating new expired time
//JWTEncoder will generate new token
func(j *JWT) RefreshToken(token string) (string, error) {
	checkTime := checkTime{}
	err := j.ParseData(token, &checkTime)
	if err != nil {
		return "", err
	}

	exp := time.Duration(checkTime.Exp)
	j.Exp = &exp

	var body = make(map[string]interface{})
	err = j.ParseData(token, &body)
	if err != nil {
		return "", err
	}
	return  j.JWTEncoder(body)
}